package com.example.java917.java10;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

public class A_Java10_Versioning implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(A_Java10_Versioning.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Runtime.Version version = Runtime.version();

        System.out.printf("""
                    Feature : %s
                    Update  : %s
                """, version.version(), version.feature());

    }
}
