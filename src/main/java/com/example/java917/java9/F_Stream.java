package com.example.java917.java9;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

public class F_Stream implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(F_Stream.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        int a = 1 == 1 ? 2 : 3;
    }
}
