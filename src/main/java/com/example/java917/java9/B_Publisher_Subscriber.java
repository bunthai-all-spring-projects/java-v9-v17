package com.example.java917.java9;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.List;
import java.util.concurrent.Flow;


public class B_Publisher_Subscriber implements CommandLineRunner {



    public static void main(String[] args) {
        SpringApplication.run(B_Publisher_Subscriber.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("============== B_Publisher_Subscriber ===============");

        Flow.Publisher<String> publisher = new Flow.Publisher() {
            @Override
            public void subscribe(Flow.Subscriber subscriber) {
                Flow.Subscription subscription = new Flow.Subscription() {
                    @Override
                    public void request(long n) {
                        System.out.println("request: " + n);
                    }

                    @Override
                    public void cancel() {
                        System.out.println("cancel");
                    }
                };
                subscriber.onSubscribe(subscription);

                List.of("a", "b", "c").forEach(val -> {
                    subscriber.onNext(val);
                });

//
//
//                subscriber.onError(new Throwable("Error"));

                subscriber.onComplete();
            }
        };

        Flow.Subscriber<String> subscriber = new Flow.Subscriber<String>() {
            @Override
            public void onSubscribe(Flow.Subscription subscription) {
                System.out.println("===== onSubscribe ======");
                subscription.request(5);
                subscription.request(10);
//                subscription.cancel();
            }

            @Override
            public void onNext(String item) {
                System.out.println("===== onNext =====");
                System.out.println("onNext Item: " + item);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {
                System.out.println("===== onComplete =====");
            }
        };

        publisher.subscribe(subscriber);

    }
}
