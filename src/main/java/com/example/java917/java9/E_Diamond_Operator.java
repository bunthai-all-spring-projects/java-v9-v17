package com.example.java917.java9;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

public class E_Diamond_Operator implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(E_Diamond_Operator.class, args);
    }


    @Override
    public void run(String... args) throws Exception {


        System.out.println("======= MyVal =========");
        MyVal<String> a = new MyVal<>("Hello");
        System.out.println(a.val);

        MyVal<Integer> b = new MyVal<>(5);
        System.out.println(b.val);


        System.out.println("======= MyKey =========");
        MyMap<String, Integer> myMap = new MyMap<>("key1", 5);
        System.out.println(myMap);

    }

    static class MyVal<T> {
        T val;

        public MyVal(T val) {
            this.val = val;
        }

    }


    static class MyMap<T, Z> {
        T key;
        Z val;

        public MyMap(T key, Z val) {
            this.key = key;
            this.val = val;
        }

        @Override
        public String toString() {
            return String.format("[%s: %d]", key, val);
        }
    }
}
