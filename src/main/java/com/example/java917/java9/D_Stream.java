package com.example.java917.java9;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class D_Stream implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(D_Stream.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        // takeWhile method takes all the values until the predicate returns false
        System.out.println("======= takeWhile =======");
        var takeWhileVals = Stream.of(1,2,3,4,0,7,5,8).takeWhile(val -> !Objects.equals(val, 0)).toList();
        System.out.println("takeWhile: " + takeWhileVals);
        // [1, 2, 3, 4]


        // dropWhile, drop mean drop value away (throw away) from start that match condition until it false
        // it will stop when hit false and return all prefix value
        // see first example, return all value because it not matched since the first index start
        System.out.println("======= dropWhile =======");
        var dropWhileVals = Stream.of(1,2,3,4,5,6,7).dropWhile(val -> val == 2).toList();
        System.out.println("dropWhile: " + dropWhileVals);
        // [1, 2, 3, 4, 5, 6, 7]


        System.out.println();
        List<Integer> numbers = Arrays.asList(1, 1, 1, 1, 2, 8, 1);
        List<Integer> result = numbers.stream()
                .dropWhile(n -> n == 1)
                .collect(Collectors.toList());
        System.out.println(result);
        // [2, 8, 1]


    }
}
