package com.example.java917.java9;

import com.example.java917.Java917Application;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class A_HttpRequest implements CommandLineRunner {



    public static void main(String[] args) {
        SpringApplication.run(A_HttpRequest.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // GET
        HttpRequest httpRequest = HttpRequest
            .newBuilder()
            .uri(URI.create("https://postman-echo.com/get"))
            .GET()
            .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());

        System.out.println(response.body());

        System.out.println("===========");


        // POST

        HttpRequest httpRequest2 = HttpRequest
            .newBuilder()
            .uri(URI.create("https://reqbin.com/echo/post/json"))
            .setHeader("content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString("""
                {
                  "Id": 12345,
                  "Customer": "John Smith",
                  "Quantity": 1,
                  "Price": 10.00
                }
                """, StandardCharsets.UTF_8))
            .build();

        HttpResponse<?> response2 = HttpClient.newHttpClient().send(httpRequest2, HttpResponse.BodyHandlers.discarding());
        System.out.println(response2.body());


    }
}
