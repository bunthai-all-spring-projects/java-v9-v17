package com.example.java917.java9;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class C_Immutable_Collection implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(C_Immutable_Collection.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        System.out.println("======== List ========");
        var listOf = List.of("a", "b", "c");
        System.out.println("listOf: " + listOf);

        System.out.println("======== Map ========");
        var mapOf = Map.ofEntries(Map.entry(1, "a"), Map.entry(2, "b"));
        var mapOf2 = Map.of("key1", "hi", "key2", "bye");
        System.out.println("mapOf: " + mapOf);
        System.out.println("mapOf2: " + mapOf2);


        System.out.println("======== Set ========");
        var setOf = Set.of("a", "a", "b", "c");
        System.out.println("setOf: " + setOf);

    }
}
